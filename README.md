# ТЗ3 Методы многомерной оптимизации

Подготовлено студентами Финансового университета при Правительстве Российской Федерации по дисциплине Оптимизационные задачи в машинном обучении. Для ознакомления с поставленной задачей нажмите [читать tz3_multidimensional_optimization.pdf](illustrations/tz3_multidimensional_optimization.pdf)

## Демонстрация возможностей

Множество примеров работы модуля доступны к просмотру любым удобным для Вас способом. <br/>
* Онлайн Google Colab Notebook - нажмите [открыть Google Colab](https://colab.research.google.com/drive/1fMVCYYyZj-z4XWp-4E_wKBd9KijG_sie?usp=sharing)
* Оффлайн Jupyter Notebook - нажмите [скачать tz3_multidimensional_optimization.ipynb](tz3_multidimensional_optimization.ipynb)

## Иллюстрация работы

* Решение и вывод <br/>
![Solution of problem](illustrations/conclusion_and_solution.png) <br/>
* Продвинутая визуализация <br/>
![Interactive visualization](illustrations/pro_visualization.gif) <br/>

## Подготовка окружения

```
# Для визуализации необходим CUDA
!nvidia-smi
!pip install GPUtil
```

## Импорт модуля

```
# Клонирование репозитория
!git clone https://leonidalekseev:maCRxpzWvEcohHKKTeG9@gitlab.com/optimization-tasks-in-machine-learning/tz2_numerical_optimization.git
!git clone https://leonidalekseev:maCRxpzWvEcohHKKTeG9@gitlab.com/optimization-tasks-in-machine-learning/tz3_multidimensional_optimization.git
# Импорт всех функций из модуля
from tz3_multidimensional_optimization.utils import *
```

## Документация функций

`help(set_function)`

```
Help on function set_function in module module tz3_multidimensional_optimization.utils:

set_function(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, functions_title: str = 'Введенная \\space функция:', functions_designation: str = 'F', is_display_input: bool = True, _is_system: bool = False) -> tuple
    Установка объекта sympy функции из строки.
    Количество переменных проверяется. Отображение функции настраивается.
    
    Parameters
    ===========
    
    input_investigated_function: str
        Входная строка с функцией
    functions_symbols: tuple, optional
        Переменные функции
    functions_title: str
        Заголовок для отображения
    functions_designation: str
        Обозначение для отображения
    is_display_input: bool
        Отображать входную функцию или нет
    _is_system: bool
        Вызов функции программой или нет
    
    Returns
    ===========
    
    input_investigated_function: str
        Входная строка с функцией
    investigated_function: sympy
        Исследуемая функция
    functions_symbols: tuple
        Переменные функции
```

`help(visualize_plotly)`

```
Help on function visualize_plotly in module tz3_multidimensional_optimization.utils:

visualize_plotly(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, points_list: Union[tuple, NoneType] = None, is_display_input: bool = True, is_autosize_graph: bool = True, start: int = -1, stop: int = 1, detail: int = 100, step: Union[float, NoneType] = None) -> None
    Визуализация функции с точками. 
    Границы графика и детализация настроивается.
    
    Parameters
    ===========
    
    input_investigated_function: str
        Исследуемая функция
    functions_symbols: tuple, optional
        Переменные функции
    points_list: tuple, optional
        Точки для визуализации
    is_display_input: bool
        Отображать входную функцию или нет
    is_autosize_graph: bool
        Автоматические устанавливать границы или нет
    start: int
        Начало графика
    stop: int
        Конец графика
    detail: int
        Детализация графика
    step: float, optional
        Единичный отрезок графика
```

`help(find_extremums)`

```
Help on function multidimensional_optimization in module tz3_multidimensional_optimization.utils:

multidimensional_optimization(method: Union[int, str], input_investigated_function: str, input_gradient_function: str = None, start_point: Union[tuple, NoneType] = None, accuracy: float = 1e-05, max_iterations: int = 500, gamma: float = 0.1, momentum: float = 0.8, crush_bias: float = 0.1, bias: Union[tuple, NoneType] = None, is_display_input: bool = True, is_display_solution: bool = True, is_save_solution: bool = False, is_display_conclusion: bool = True, is_try_visualize: bool = False) -> tuple
    Оптимизация многомерной функции с заданными параметрами. 
    
    Parameters
    ===========
    
    method: int, str
        Методы оптимизации
        0, 'constant': метод градиентного спуска с постоянным шагом
        1, 'step_crush': метод градиентного спуска с дробленим шага
        2, 'optimal': метод наискорейшего градиентного спуска
        3, 'co_newton': метод Ньютон-сопряженного градиентного спуска
        4, 'lr_momentum': метод градиентного спуска с lr и momentum
        
    input_investigated_function: str
        Входная строка с функцией
    input_gradient_function: str, optional
        Входная строка с функцией градиента
    start_point: tuple, optional
        Начальная точка в методе 3
    accuracy: float
        Точность во всех методах
    max_iterations: int
        Максимум итераций во всех методах
    gamma: float
        Коэфициент силы смещения (шага)
    momentum: float
        Коэфициент импульса для метода 4
    crush_bias: float
        Коэфициент дробления шага для метода 1
    bias: tuple, optional
        Начальные значения смещения (шага)
    is_display_input: bool = True
        Отображать входную функцию или нет
    is_display_solution: bool = True
        Отображать решение или нет
    is_save_solution: bool = False
        Сохранять решение или нет
    is_display_conclusion: bool = True
        Отображать вывод или нет               
    is_try_visualize: bool = False
        Визуализация процесса
    
    Returns
    ===========
    
    function_point: numpy.float64
        Найденая точка экстремума
    function_value: numpy.float64
        Значение функции в точке экстремума
    flag_process: int
        Флаг завершения алгоритма
        0: найдено значение с заданной точностью
        1: достигнуто максимальное количество итераций
        2: выполнено с ошибкой
```

## Участники проекта

* [Быханов Никита](https://gitlab.com/BNik2001) - Менеджер проектa, Тестировщик
* [Алексеев Леонид](https://gitlab.com/LeonidAlekseev) - Программист, Тестировщик
* [Семёнова Полина](https://gitlab.com/polli_eee) - Аналитик, Тестировщик
* [Янина Марина](https://gitlab.com/marinatdd) - Аналитик, Тестировщик
* [Буркина Елизавета](https://gitlab.com/lizaburkina) - Аналитик
* [Егорин Никита](https://gitlab.com/hadesm8) - Аналитик

`Группа ПМ19-1`

## Используемые источники

* [Sympy documentation](https://www.sympy.org/en/index.html)
* [Plotly documentation](https://plotly.com/python/)
